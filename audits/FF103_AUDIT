# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `feef2b7da6ff76b4c3c7a13e611eab6e97837ae2` ( `FIREFOX_102_0_1_RELEASE` )
- End:   `1f1c56dc6bae6b3302471f097ed132ef44cded86`  ( `FIREFOX_103_0_2_RELEASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `0302b89604bb29adb34fdcd710feabd3dd01992d` ( `v93.5.0` )
- End:   `b70c54882fec606d10e77520b1dd2ae144768747`  ( `v94.0.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Android Components: https://github.com/mozilla-mobile/android-components.git

- Start: `c349019286e21e5179a59e33cec6ae528b108519`
- End:   `ce720e6308e26588decd5f9d2b02e4658d5b9d14`  ( `v103.0.11` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

<!--
### foreach PROBLEMATIC_HASH:
#### $(PROBLEMATIC_HASH)
- Summary
- Review Result: (SAFE|BAD)
-->

#### Problematic Commits
- Biometric prompt feature (#12291): `b1723820859fce1c6d0a358e1edec4f4dec39234`

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `6e0ad035e384ef1fb7d8c8ac8d398470d953e1dc` ( `v103.0.0-beta.1` )
- End:   `e0d3c03afefe699a7456214b0da140f74ef499f7`  ( `v103.2.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=103%20Branch&order=priority%2Cbug_severity&limit=0`

#### Problematic Tickets
- **Use double-conversion library instead of dtoa for string-to-double conversion** https://bugzilla.mozilla.org/show_bug.cgi?id=1770158
  - Uses upstream https://github.com/google/double-conversion from google
  - Well tested and documentaiton doesn't suggest any hardware specific differences, quick look through the source suggest integer operations for the parsing/conversions
