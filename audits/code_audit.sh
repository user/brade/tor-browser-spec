#!/bin/bash -e

# set -x

if [ $# -ne 4 ]; then
    echo "usage: <path/to/repo> <old commit> <new commit> <lang>"
    exit 1
fi

REPO_DIR=$1

OLD=$2
NEW=$3
SCOPE=$4

declare -a KEYWORDS

#KEYWORDS+=('\+\+\+\ ')

initialize_java_symbols() {
    # URL access
    KEYWORDS+=(URLConnection)
    KEYWORDS+=(UrlConnectionDownloader)

    # Proxy settings
    KEYWORDS+=(ProxySelector)

    # Android and java networking and 3rd party libs
    KEYWORDS+=("openConnection\(")
    KEYWORDS+=("java.net")
    KEYWORDS+=("javax.net")
    KEYWORDS+=(android.net)
    KEYWORDS+=(android.webkit)

    # Third Party http libs
    KEYWORDS+=(ch.boye.httpclientandroidlib.impl.client)
    KEYWORDS+=(okhttp)

    # Intents
    KEYWORDS+=(IntentHelper)
    KEYWORDS+=(openUriExternal)
    KEYWORDS+=(getHandlersForMimeType)
    KEYWORDS+=(getHandlersForURL)
    KEYWORDS+=(getHandlersForIntent)
    # KEYOWRDS+=(android.content.Intent) # Common
    KEYWORDS+=(startActivity)
    KEYWORDS+=(startActivities)
    KEYWORDS+=(startBroadcast)
    KEYWORDS+=(sendBroadcast)
    KEYWORDS+=(sendOrderedBroadcast)
    KEYWORDS+=(startService)
    KEYWORDS+=(bindService)
    KEYWORDS+=(android.app.PendingIntent)
    KEYWORDS+=(ActivityHandlerHelper.startIntentAndCatch)
    KEYWORDS+=(AppLinksInterceptor)
    KEYWORDS+=(AppLinksUseCases)
    KEYWORDS+=(ActivityDelegate)
    # Added in FF87 audit
    KEYWORDS+=(AutofillService)
    # Added in FF88 audit
    KEYWORDS+=(AutofillConfiguration)
    KEYWORDS+=(Authenticator)
    KEYWORDS+=(AutofillUnlockActivity)
}

initialize_rust_symbols() {
    KEYWORDS+=("connect\(")
    KEYWORDS+=("recvmsg\(")
    KEYWORDS+=("sendmsg\(")
    KEYWORDS+=("::post\(")
    KEYWORDS+=("::get\(")
}

initialize_cpp_symbols() {
    KEYWORDS+=("PR_GetHostByName")
    KEYWORDS+=("PR_GetIPNodeByName")
    KEYWORDS+=("PR_GetAddrInfoByName")
    KEYWORDS+=("PR_StringToNetAddr")

    KEYWORDS+=("MDNS")
    KEYWORDS+=("mDNS")
    KEYWORDS+=("mdns")

    KEYWORDS+=("TRR")
    KEYWORDS+=("trr")

    KEYWORDS+=("AsyncResolve")
    KEYWORDS+=("asyncResolve")
    KEYWORDS+=("ResolveHost")
    KEYWORDS+=("resolveHost")

    KEYWORDS+=("SOCK_")
    KEYWORDS+=("SOCKET_")
    KEYWORDS+=("_SOCKET")

    KEYWORDS+=("UDPSocket")
    KEYWORDS+=("TCPSocket")

    KEYWORDS+=("PR_Socket")

    KEYWORDS+=("SocketProvider")
    KEYWORDS+=("udp-socket")
    KEYWORDS+=("tcp-socket")
    KEYWORDS+=("tcpsocket")
    KEYWORDS+=("SOCKET")
    KEYWORDS+=("mozilla.org/network")
}

initialize_js_symbols() {
    KEYWORDS+=("AsyncResolve\(")
    KEYWORDS+=("asyncResolve\(")
    KEYWORDS+=("ResolveHost\(")
    KEYWORDS+=("resolveHost\(")

    KEYWORDS+=("udp-socket")
    KEYWORDS+=("udpsocket")
    KEYWORDS+=("tcp-socket")
    KEYWORDS+=("tcpsocket")
    KEYWORDS+=("SOCKET")
    KEYWORDS+=("mozilla.org/network")
}

# Step 1: Initialize scope of audit
EXT=
case "${SCOPE}" in
    "java" | "kt" | "java-kt" )
        EXT="java kt"
        SCOPE="java-kt"
        initialize_java_symbols
        ;;
    "c-cpp" | "c-cxx" | "c" | "cxx" | "cpp" )
        EXT="c cpp h cxx hpp hxx cc hh"
        SCOPE="c-cpp"
        initialize_cpp_symbols
        ;;
    "rust" )
        EXT="rs"
        initialize_rust_symbols
        ;;
    "js" )
        EXT="js jsm"
        initialize_js_symbols
        ;;
    * )
        echo "requested language not recognized"
        exit 1
        ;;
esac

AUDIT_DIR=$(pwd)
pushd "$REPO_DIR"

# Step 2: Generate match pattern based on in-scope keywords
function join_by { local d=$1; shift; local f=$1; shift; printf %s "$f" "${@/#/$d}"; }
GREP_LINE="$(join_by \| "${KEYWORDS[@]}")"

# Step 3: Obtain patches for all in-scope files where a keyword is present
declare -a path
for ext in ${EXT}; do
    path+=("*.${ext}")
done
PROJECT_NAME=$(basename $(pwd))
REPORT_FILE="$AUDIT_DIR/$PROJECT_NAME-$SCOPE-${OLD:0:8}-${NEW:0:8}.report"

echo "Diffing all ${path[*]} files in commit range ${OLD:0:8}..${NEW:0:8}"
# Exclude Deleted and Unmerged files from diff
DIFF_FILTER=ACMRTXB

rm -f "${REPORT_FILE}"

# Step 4: Concat a diff of each commit containing keyword and the commit message/hash
# of said commit

# Flashing Color constants
export GREP_COLOR="05;37;41"

for COMMIT in $(git rev-list --ancestry-path $OLD~..$NEW); do
    TEMP_DIFF="$(mktemp)"

    echo "Diffing $COMMIT..."

    # Do each diff commit by commit so we can add context from the commit log
    # to each diff
    git diff --stat --color=always --color-moved --diff-filter="${DIFF_FILTER}" -U20 -G"${GREP_LINE}" $COMMIT~ $COMMIT -- "${path[@]}" > "${TEMP_DIFF}"
    if [ -s "${TEMP_DIFF}" ]
    then
        #  Highlight the keyword with an annoying, flashing color
        FLASHING_DIFF="$(mktemp)"
        grep -A10000 -B10000 --color=always -E "${GREP_LINE}" "${TEMP_DIFF}" > "${FLASHING_DIFF}"
        mv "${FLASHING_DIFF}" "${TEMP_DIFF}"

        # Add a 'XXX MATCH XXX' at the end of each matched line, easily searchable.
        sed -i 's/\(\x1b\[05;37;41.*\)/\1    XXX MATCH XXX/' "${TEMP_DIFF}"

        # Found some diff, so cat the changelog for commit then the diff
        echo "-----------------------------------------------" >> "${REPORT_FILE}"
        git log -n 1 $COMMIT >> "${REPORT_FILE}"
        echo "-----------------------------------------------" >> "${REPORT_FILE}"
        cat "${TEMP_DIFF}" >> "${REPORT_FILE}"
    fi

    rm -f "${TEMP_DIFF}"
done
popd

# Step 5: Review the code changes

if [ -s "${REPORT_FILE}" ]
then
    echo ""
    echo "Report generated. View it with:"
    echo ""
    echo "less -R \"$(basename "${REPORT_FILE}")\""
    less -R "$(basename ${REPORT_FILE})"
else
    echo "No keywords found. No report generated"
fi

