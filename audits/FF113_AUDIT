# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `8307d3d3e4bfbca09aaa17e444f106e1e1d91b65` ( `FIREFOX_112_0_2_RELEASE` )
- End:   `8a724297d78ba792067a7e4d17d170c6b3af796e`  ( `FIREFOX_113_0_2_RELEASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `48916bbaf585f89fdff3404d181b260ed981a2d6`  ( `v97.5.0` )
- End:   `e3db1107890d3fe69122d4d78ae04c857329b0ea`  ( `v114.1` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Firefox Android: https://github.com/mozilla-mobile/firefox-android.git

- Start: `32bb398127bacccb268a272091d2e62b8d72d6b9`
- End:   `9d87757910437e94a860e1d6f2577d5648ded966`

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

#### Problematic Commits

- Bug 1822268 - Part 1: Add juno onboarding fragment `536df038039d256cd41ecad41df97b1b3da1a2e4`
- Bug 1821726 - Part 2: Add juno onboarding telemetry `db4bc04177766a241f1e36621c1d8c480aab568d`
- Bug 1822750 - Add option to open default browser help page in custom tab `a488c06ae9ff9d060517da08e2a2db94465dc871`
  - **RESOLUTION** All three of these are part of the Firefox onboarding which we have disabled entirely!

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=113%20Branch&order=priority%2Cbug_severity&limit=0`

Nothing of interest (manual inspection)

#### Problematic Tickets

- **Allow users to submit site support requests in Fenix**  https://bugzilla.mozilla.org/show_bug.cgi?id=1805450
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42007
  - **RESOLUTION** disabled
- **Copying images from Pixiv and pasting them in certain programs is broken** https://bugzilla.mozilla.org/show_bug.cgi?id=1808146
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42008
  - **RESOLUTION** verified the web request happens in Firefox not the destination app
- **Enable overscroll on Windows on all channels** https://bugzilla.mozilla.org/show_bug.cgi?id=1810641
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42010
  - **RESOLUTION** nothing to do here until we enable touch events (disabled for now)
## Export
- [ ] Export Report and save to `tor-browser-spec/audits`